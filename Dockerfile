FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/sgps/src
COPY pom.xml /home/sgps
RUN mvn -f /home/sgps/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/sgps/target/sgps.jar /usr/local/lib/sgps.jar
EXPOSE 9001
ENTRYPOINT ["java","-jar","/usr/local/lib/sgps.jar"]