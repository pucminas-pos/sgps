package br.com.boasaude.sgps.application.controller;

import br.com.boasaude.sgps.domain.model.internal.Associate;
import br.com.boasaude.sgps.domain.external.sgpsgateway.LoginRequestDTO;
import br.com.boasaude.sgps.domain.service.AssociateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("associates")
public class AssociateController {

    private final AssociateService associateService;


    @PostMapping("login")
    public ResponseEntity login(@RequestBody LoginRequestDTO loginRequestDTO) {
        Associate associate = associateService.login(loginRequestDTO);
        return ResponseEntity.ok(associate);
    }

    @PostMapping
    public ResponseEntity save(@RequestBody Associate associate) {
        Associate associatePersisted = associateService.save(associate);
        return ResponseEntity.status(HttpStatus.CREATED).body(associatePersisted);
    }

    @GetMapping("email")
    public ResponseEntity findByEmail(@RequestParam("email") String email) {
        return ResponseEntity.ok(associateService.findByEmail(email));
    }
}
