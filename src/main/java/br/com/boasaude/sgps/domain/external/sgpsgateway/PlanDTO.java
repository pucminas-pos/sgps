package br.com.boasaude.sgps.domain.external.sgpsgateway;

import br.com.boasaude.sgps.domain.model.internal.PlanName;
import br.com.boasaude.sgps.domain.model.internal.Status;
import br.com.boasaude.sgps.domain.model.internal.PlanType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanDTO {

    private String number;
    private PlanName name;
    private PlanType type;
    private Status status;
}
