package br.com.boasaude.sgps.domain.model.internal;

public enum Status {

    ACTIVE, SUSPENDED, INACTIVE

}
