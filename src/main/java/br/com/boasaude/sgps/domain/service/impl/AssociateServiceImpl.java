package br.com.boasaude.sgps.domain.service.impl;

import br.com.boasaude.sgps.domain.external.sgpsgateway.LoginRequestDTO;
import br.com.boasaude.sgps.domain.model.internal.Associate;
import br.com.boasaude.sgps.domain.model.internal.Status;
import br.com.boasaude.sgps.domain.service.AssociateService;
import br.com.boasaude.sgps.infrastructure.exception.AssociateNotFoundException;
import br.com.boasaude.sgps.infrastructure.exception.LoginIncorrectException;
import br.com.boasaude.sgps.infrastructure.repository.AssociateRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AssociateServiceImpl implements AssociateService {

    private final AssociateRepository associateRepository;


    @Override
    public Associate findByEmail(String email) {
        return associateRepository.findByEmail(email).orElseThrow(() -> {
            throw new AssociateNotFoundException("associate not found");
        });
    }

    @Override
    public Associate login(LoginRequestDTO loginRequestDTO) {
        return associateRepository.findByEmailAndPassword(loginRequestDTO.getEmail(), DigestUtils.md5Hex(loginRequestDTO.getPassword())).orElseThrow(() -> {
            throw new LoginIncorrectException("email or password incorrect");
        });
    }

    @Override
    @Transactional
    public Associate save(Associate associate) {
        return associateRepository.save(associate);
    }
}
