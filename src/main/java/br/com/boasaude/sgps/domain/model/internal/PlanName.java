package br.com.boasaude.sgps.domain.model.internal;

public enum PlanName {

    INDIVIDUAL, CORPORATE;

}
