package br.com.boasaude.sgps.domain.model.internal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@Entity(name = "plans")
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Plan implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PlanName name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PlanType type;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PlanCategory category;
}
