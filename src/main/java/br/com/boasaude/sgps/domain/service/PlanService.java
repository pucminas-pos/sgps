package br.com.boasaude.sgps.domain.service;

import br.com.boasaude.sgps.domain.model.internal.Plan;
import br.com.boasaude.sgps.domain.model.internal.PlanCategory;
import br.com.boasaude.sgps.domain.model.internal.PlanName;
import br.com.boasaude.sgps.domain.model.internal.PlanType;

import java.util.List;

public interface PlanService {

    List<Plan> findAll();

    Plan findById(Long id);

    Plan findByFilter(PlanName name, PlanCategory category, PlanType type);
}
