package br.com.boasaude.sgps.domain.service;

import br.com.boasaude.sgps.domain.model.internal.Associate;
import br.com.boasaude.sgps.domain.external.sgpsgateway.LoginRequestDTO;

public interface AssociateService {

    Associate findByEmail(String email);

    Associate login(LoginRequestDTO loginRequestDTO);

    Associate save(Associate associate);
}
