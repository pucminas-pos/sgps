package br.com.boasaude.sgps.domain.model.internal;

public enum PlanType {

    MEDICAL, MEDICAL_ODONTOLOGY;

}
