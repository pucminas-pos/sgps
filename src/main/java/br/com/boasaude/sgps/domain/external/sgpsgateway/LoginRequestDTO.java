package br.com.boasaude.sgps.domain.external.sgpsgateway;

import br.com.boasaude.sgps.domain.model.internal.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequestDTO {

    private String email;
    private String password;
    private UserRole role;
}
