package br.com.boasaude.sgps.domain.service.impl;

import br.com.boasaude.sgps.domain.model.internal.Plan;
import br.com.boasaude.sgps.domain.model.internal.PlanCategory;
import br.com.boasaude.sgps.domain.model.internal.PlanName;
import br.com.boasaude.sgps.domain.model.internal.PlanType;
import br.com.boasaude.sgps.domain.service.PlanService;
import br.com.boasaude.sgps.infrastructure.exception.PlanNotFoundException;
import br.com.boasaude.sgps.infrastructure.repository.PlanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlanServiceImpl implements PlanService {

    private final PlanRepository planRepository;


    @Override
    public List<Plan> findAll() {
        return Streamable.of(planRepository.findAll()).toList();
    }

    @Override
    public Plan findById(Long id) {
        return planRepository.findById(id).orElseThrow(() -> {
            throw new PlanNotFoundException("plan not found exception");
        });
    }

    @Override
    public Plan findByFilter(PlanName name, PlanCategory category, PlanType type) {
        return planRepository.findByNameAndCategoryAndType(name, category, type).orElseThrow(() -> {
            throw new PlanNotFoundException("plan not found exception");
        });
    }
}
