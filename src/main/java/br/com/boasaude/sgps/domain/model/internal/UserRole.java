package br.com.boasaude.sgps.domain.model.internal;

public enum UserRole {

    ADMIN, ASSOCIATE
}
