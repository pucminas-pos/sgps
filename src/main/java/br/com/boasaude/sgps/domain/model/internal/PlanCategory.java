package br.com.boasaude.sgps.domain.model.internal;

public enum PlanCategory {

    BASIC, INTERMEDIATE, VIP

}
