package br.com.boasaude.sgps.infrastructure.repository;

import br.com.boasaude.sgps.domain.model.internal.Plan;
import br.com.boasaude.sgps.domain.model.internal.PlanCategory;
import br.com.boasaude.sgps.domain.model.internal.PlanName;
import br.com.boasaude.sgps.domain.model.internal.PlanType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlanRepository extends CrudRepository<Plan, Long> {

    Optional<Plan> findByNameAndCategoryAndType(PlanName name, PlanCategory category, PlanType type);

}
