package br.com.boasaude.sgps.infrastructure.exception;

public class PlanStatusNotActiveException extends RuntimeException {

    public PlanStatusNotActiveException() {
    }

    public PlanStatusNotActiveException(String message) {
        super(message);
    }

    public PlanStatusNotActiveException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlanStatusNotActiveException(Throwable cause) {
        super(cause);
    }
}
