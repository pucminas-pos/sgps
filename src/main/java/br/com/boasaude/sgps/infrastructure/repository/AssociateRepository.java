package br.com.boasaude.sgps.infrastructure.repository;

import br.com.boasaude.sgps.domain.model.internal.Associate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AssociateRepository extends CrudRepository<Associate, Long> {

    Optional<Associate> findByEmail(String email);

    Optional<Associate> findByEmailAndPassword(String email, String password);

}
