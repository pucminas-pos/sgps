package br.com.boasaude.sgps.infrastructure.exception;

public class MigrationForTheSamePlanException extends RuntimeException {

    public MigrationForTheSamePlanException() {
    }

    public MigrationForTheSamePlanException(String message) {
        super(message);
    }

    public MigrationForTheSamePlanException(String message, Throwable cause) {
        super(message, cause);
    }

    public MigrationForTheSamePlanException(Throwable cause) {
        super(cause);
    }
}
