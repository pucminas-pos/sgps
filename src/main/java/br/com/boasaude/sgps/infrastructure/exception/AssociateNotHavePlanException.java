package br.com.boasaude.sgps.infrastructure.exception;

public class AssociateNotHavePlanException extends RuntimeException {

    public AssociateNotHavePlanException() {
    }

    public AssociateNotHavePlanException(String message) {
        super(message);
    }

    public AssociateNotHavePlanException(String message, Throwable cause) {
        super(message, cause);
    }

    public AssociateNotHavePlanException(Throwable cause) {
        super(cause);
    }
}
